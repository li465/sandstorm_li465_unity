﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CandyCane : MonoBehaviour {
	//get a reference from the playerShooting script
	PlayerShooting playerShooting;


	// Use this for initialization
	//how can I get a reference of the player                                                                                                                                                                                                    
	void Awake () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider other){
		if (other.CompareTag ("Player")) {
			playerShooting = other.GetComponent <PlayerMovement> ().myPlayerShooting;
			playerShooting.addBullet ();

			Destroy (gameObject);

			//call the addBullet function in PlayerShooting script
			//playerShooting.addBullet ();

		}
	
	}
}
