﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterTime : MonoBehaviour {

	public float myTimer = 30f;

	// Use this for initialization
	void Start () {
		Invoke ("destroySpan", myTimer);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void destroySpan(){
		GameObject.Destroy (gameObject);
	}
}
