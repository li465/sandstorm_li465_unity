﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
	public float speed = 6f;
	public PlayerShooting myPlayerShooting;
	public Slider StarSlider;
	public Text StarText;
	public GameObject snowflakeWall;




	Vector3 movement;
	Animator anim;
	Rigidbody playerRigidbody;
	int floorMask;
	float camRayLength = 100f;
	public int starCount = 0;
	float speedTime;


	void Update(){
		if (Input.GetKeyDown (KeyCode.K) && starCount >= 5) {
			print ("HELLO!");
			myPlayerShooting.starPower ();
			StarText.gameObject.SetActive (false);
			StarSlider.value = 0;
		}

		if (speedTime > 0f) {
			speedTime -= Time.deltaTime;
			print (speedTime);
		}
		else {
			speed = 6f;
		}


	}

	void Awake(){
		//Set up the references
		floorMask = LayerMask.GetMask ("Floor");
		anim = GetComponent <Animator> ();
		playerRigidbody = GetComponent<Rigidbody> ();
	}

	void FixedUpdate(){
		//Player's key wasd movement update
		float h = Input.GetAxisRaw ("Horizontal");
		float v = Input.GetAxisRaw ("Vertical");

		Move (h, v);
		Turning ();
		Animating (h, v);
	}

	void Move(float h, float v){
		movement.Set(h, 0f, v);

		movement = movement.normalized * speed * Time.deltaTime;

		playerRigidbody.MovePosition (transform.position + movement);
	}

	void Turning() {
		Ray camRay = Camera.main.ScreenPointToRay (Input.mousePosition);

		RaycastHit floorHit;

		if (Physics.Raycast (camRay, out floorHit, camRayLength, floorMask)) {
			Vector3 playerToMouse = floorHit.point - transform.position;
			playerToMouse.y = 0f;

			Quaternion newRotation = Quaternion.LookRotation (playerToMouse);
			playerRigidbody.MoveRotation (newRotation);
		
		}


	}

	void Animating(float h, float v){
		bool walking = h != 0f || v != 0f;

		anim.SetBool ("IsWalking", walking);
	}



	void OnTriggerEnter(Collider other){
		if (other.CompareTag ("Star")) {
			GameObject.Destroy (other.gameObject);

			starCount += 1;
			StarSlider.value = starCount;
		} else if (other.CompareTag ("snowflake")) {
			Vector2 randomCircleDist = Random.insideUnitCircle.normalized * 3f;

			Instantiate (snowflakeWall, new Vector3 (transform.position.x + randomCircleDist.x, transform.position.y + 1.2f, transform.position.z + randomCircleDist.y), snowflakeWall.transform.rotation);

		
		} else if (other.CompareTag ("car")) {
			GameObject.Destroy (other.gameObject);

			speed = 12f;

			speedTime = 5f;

		}else if (other.CompareTag ("Present")) {
			return;
		}
	}
		


}
