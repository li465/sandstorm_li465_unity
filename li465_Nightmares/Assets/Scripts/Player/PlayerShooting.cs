﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerShooting : MonoBehaviour
{
    public int damagePerShot = 20;
    public float timeBetweenBullets = 0.15f;
    public float range = 100f;
	public Slider bulletSlider;
	public PlayerMovement myPlayerMovement;

    float timer;
	float starTime;
    Ray shootRay = new Ray();
    RaycastHit shootHit;
    int shootableMask;
    ParticleSystem gunParticles;
    LineRenderer gunLine;
    AudioSource gunAudio;
    Light gunLight;
	float effectsDisplayTime = 0.2f;
	int bulletCount = 100;


    void Awake ()
    {
        shootableMask = LayerMask.GetMask ("Shootable"); //numbers of shootable layers
        gunParticles = GetComponent<ParticleSystem> ();
        gunLine = GetComponent <LineRenderer> ();
        gunAudio = GetComponent<AudioSource> ();
        gunLight = GetComponent<Light> ();
    }


    void Update ()
    {

		if (starTime > 0f) {
			starTime -= Time.deltaTime;
		}
		else {
			gunLight.color = Color.yellow;
			damagePerShot = 20;
		}

        timer += Time.deltaTime;

		if(Input.GetButton ("Fire1") && timer >= timeBetweenBullets && Time.timeScale != 0)
        {
            Shoot ();
        }

        if(timer >= timeBetweenBullets * effectsDisplayTime)
        {
            DisableEffects ();
        }
			

    }


    public void DisableEffects ()
    {
        gunLine.enabled = false;
        gunLight.enabled = false;
    }


    void Shoot ()
	{
		if (bulletCount == 0)
			DisableEffects ();
		else {
			timer = 0f;

			gunAudio.Play ();

			gunLight.enabled = true;

			gunParticles.Stop ();
			gunParticles.Play ();

			gunLine.enabled = true;
			gunLine.SetPosition (0, transform.position);

			shootRay.origin = transform.position;
			shootRay.direction = transform.forward;

			if (Physics.Raycast (shootRay, out shootHit, range, shootableMask)) {
				EnemyHealth enemyHealth = shootHit.collider.GetComponent <EnemyHealth> ();
				if (enemyHealth != null) {
					enemyHealth.TakeDamage (damagePerShot, shootHit.point);
				}
				gunLine.SetPosition (1, shootHit.point);
			} else {
				gunLine.SetPosition (1, shootRay.origin + shootRay.direction * range);
			}

			// decrease the bullet slider with every shoots;
			bulletCount -= 1;
			bulletSlider.value = bulletCount;
		}
	}

	//add bullets from other trigger / event happened in other script
	public void addBullet(){
		bulletCount += 30;
		bulletSlider.value = bulletCount;
	}

	public void starPower(){
		gunLight.color = Color.red;

		damagePerShot = 50;
			
		myPlayerMovement.starCount = 0;

		starTime = 20f;



	}



}
