﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class star : MonoBehaviour {
	public Slider starSlider;
	public Text starText;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider other){
		if (other.CompareTag ("Player")) {
			Destroy (gameObject);

			starSlider = other.GetComponent <PlayerMovement> ().StarSlider;
			starText = other.GetComponent <PlayerMovement> ().StarText;

			if (starSlider.value < 5) {
				other.GetComponent <PlayerMovement> ().starCount += 1;
				starSlider.value = other.GetComponent <PlayerMovement> ().starCount;
				print ("StarCount: " + starSlider.value);
				if (starSlider.value >= 5) {
					starText.gameObject.SetActive (true);
				}
			}

		}
	}


}
